import { ClickCountRangeModel } from '../../../db/models/click';
import * as service from '../../services/clickService';

export const getCountByLinkId = async(req: string): Promise<number> => {
    return await service.getCountByLinkId(req);
}

export const getCountByRange = async(req: ClickCountRangeModel): Promise<number> => {
    return await service.getCountByRange(req);
}