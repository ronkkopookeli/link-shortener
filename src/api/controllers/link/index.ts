import { LinkAddModel, LinkViewModel } from "../../../db/models/link";
import * as service from '../../services/linkService';

export const create = async(req: string): Promise<LinkAddModel> => {
    return await service.create(req);
}

export const getByShort = async(req: string): Promise<LinkViewModel> => {
    return await service.getByShort(req);
}

export const deleteById = async(req: string): Promise<Boolean> => {
    const isDeleted = await service.DeleteById(req);
    return isDeleted;
}