import { Router, Request, Response } from 'express';
import * as clickController from  '../controllers/click';

const clicksRouter = Router();

clicksRouter.get('/:id', async(req: Request, res: Response) => {
    const short = req.params.id;
    const result = await clickController.getCountByLinkId(short);
    return res.send({count: result});
});

clicksRouter.get('/:id/range/:range?', async(req: Request, res: Response) => {
    const payload = {
        linkId: req.params.id,
        range: (req.params.range === undefined && Number.isNaN(req.params.range)) ? 
            null: 
            Number.parseInt(req.params.range, 10)
    }
    console.log(payload.range);
    const result = await clickController.getCountByRange(payload);
    return res.send({count: result});
});

export default clicksRouter;