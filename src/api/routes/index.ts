import { Router } from "express";
import linksRouter from "./link";
import clicksRouter from './click';

const router = Router();

router.use('/links', linksRouter);
router.use('/clicks', clicksRouter);

export default router;