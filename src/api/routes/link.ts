import { Router, Request, Response } from "express";
import * as linkController from  '../controllers/link';

const linksRouter = Router();

linksRouter.post('/', async(req: Request, res: Response) => {
    const link = req.body.url;
    const result = await linkController.create(link);
    return res.status(200).send(result);
});

linksRouter.get('/:id', async(req: Request, res: Response) => {
    const short = req.params.id;
    const result = await linkController.getByShort(short);
    return res.status(200).send(result);
});

linksRouter.delete('/:id', async(req: Request, res: Response) => {
    const id = req.params.id;
    const result = await linkController.deleteById(id);
    return res.status(204).send({
        success: result
    });
});

export default linksRouter;