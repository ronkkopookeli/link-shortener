import { ClickCountRangeModel } from '../../db/models/click';
import * as clickDal from '../../db/dal/click';
import * as linkDal from '../../db/dal/link';

export const getCountByLinkId = async (id: string): Promise<number> => {
    return clickDal.getCountByLinkId(id);
}

export const getCountByRange = async (req: ClickCountRangeModel): Promise<number> => {
    return req.range ? 
        clickDal.getCountByRange(req.linkId, req.range) : 
        clickDal.getCountByRange(req.linkId);
}