
import { LinkAddModel, LinkViewModel } from '../../db/models/link';
import * as linkDal from '../../db/dal/link';
import * as clickDal from '../../db/dal/click';
import { randomBytes } from 'crypto';

export const create = (url: string): Promise<LinkAddModel> => {
    return linkDal.create({
        url: url,
        short: randomBytes(6).toString('base64').substr(0,6),
        long: randomBytes(24).toString('hex').substr(0,24)
    });
}

export const getByShort = async (id: string): Promise<LinkViewModel> => {
    const link = await linkDal.getByShort(id);
    clickDal.create({linkId: link.long});
    return {
        url: link.url,
        short: link.short
    };
}

export const DeleteById = async (id: string): Promise<boolean> => {
    if(await clickDal.deleteByLong(id)) {
        return linkDal.deleteById(id);
    } else {
        return false;
    }
}