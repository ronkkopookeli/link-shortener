import { Sequelize } from "sequelize/dist";

const dbName = 'docker';
const dbUser = 'docker';
const dbHost = 'localhost';
const dbDriver = 'postgres';
const dbPassword = 'docker';

const sequelizeConnection = new Sequelize(dbName, dbUser, dbPassword, {
  host: dbHost,
  dialect: dbDriver
});

export default sequelizeConnection;
