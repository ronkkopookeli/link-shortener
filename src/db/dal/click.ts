import { Op } from "sequelize/dist";
import Click, { ClickAddModel, ClickViewModel } from "../models/click";

export const create = async (payload: ClickAddModel): Promise<ClickViewModel> => {
    const link = await Click.create(payload);
    return link;
}

export const getCountByLinkId = async (id: string): Promise<number> => {
    const count = await Click.count({
        where: {
            linkId: id
        }
    });
    return count;
}

export const getCountByRange = async (id: string, range: number = 1): Promise<number> => {
    const today = new Date();
    const yesterday = new Date(today);
    yesterday.setDate(yesterday.getDate() - range);
    const count = await Click.count({
        where: { 
            [Op.and]: [
                {linkId: id},
                {
                    createdAt: {
                        [Op.lt]: today,
                        [Op.gt]: yesterday
                    }
                }
            ]
        }
    });
    return count;
}

export const deleteByLong = async (long: string): Promise<boolean> => {
    const deletedCount = await Click.destroy({
        where: {
            linkId: long
        }
    });
    return !!deletedCount
}