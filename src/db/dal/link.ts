import Link, { LinkAddModel } from "../models/link"

export const create = async (payload: LinkAddModel): Promise<LinkAddModel> => {
    const link = await Link.create(payload);
    return link;
}

export const getByShort = async (id: string): Promise<LinkAddModel> => {
    const link = await Link.findOne({
        where: { 
            short: id 
        }
    });
    if (!link) {
        throw new Error('No URL was found for given short: ' + id);
    }
    return link;
}

export const deleteById = async (id: string): Promise<boolean> => {
    const deletedCount = await Link.destroy({
        where: {
            long: id
        }
    });
    return !!deletedCount;
}
