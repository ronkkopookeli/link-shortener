import Click from "./models/click";
import Link from "./models/link";

const isDev = process.env.NODE_ENV == 'development';

const dbInit = () => {
    Link.sync({ alter: isDev });
    Click.sync({ alter: isDev });
}

export default dbInit;