import { DataTypes, Model } from "sequelize/dist";
import sequelizeConnection from "../config";

export interface ClickAddModel {
    linkId: string
}

export interface ClickViewModel {
    id: number,
    linkId: string
}

export interface ClickCountRangeModel {
    linkId: string,
    range?: number
}

export class Click extends Model<ClickAddModel> implements ClickAddModel{
    public id!: number;
    public linkId!: string;

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
}

Click.init(
    {
        linkId: {
            type: DataTypes.STRING(),
            references: {
                model: 'Links',
                key: 'long'
            }
        }
    }, {
        timestamps: true,
        sequelize: sequelizeConnection
    }
);

export default Click;