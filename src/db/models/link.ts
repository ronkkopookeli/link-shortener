import { DataTypes, Model } from "sequelize/dist";
import sequelizeConnection from '../config'
import { Click } from "./click";

// return when creating short link
// user needs long to access admin
export interface LinkAddModel {
    long: string,
    short: string,
    url: string
}

// return when using short link
// we dont want to expose long
export interface LinkViewModel {
    short: string,
    url: string
};

// db model
export class Link extends Model<LinkAddModel> implements LinkAddModel {
    public long!: string;
    public short!: string;
    public url!: string;

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
}

Link.init(
    {
        long: {
            type:DataTypes.STRING(24),
            primaryKey: true
        },
        short: {
            type: DataTypes.STRING(6),
            allowNull: false
        },
        url: {
            type: DataTypes.STRING(2048),
            allowNull: false
        }
    }, {
        timestamps: true,
        sequelize: sequelizeConnection
    }
);

Link.hasMany(Click);

export default Link;