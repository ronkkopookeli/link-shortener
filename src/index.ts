import bodyParser from "body-parser";
import express, { Application } from "express";
import routes from './api/routes';
import dbInit from "./db/init";
import cors from 'cors';

dbInit();
const port = 8080;
const corsOptions = {
    origin: '*',
    Headers: 'X-Requested-With'
  };

export const get = () => {

    const app: Application = express();

    app.get( "/", ( req, res ) => {
        console.log(req);
        res.send( "Hello world!" );
    } );

    app.use(bodyParser.json());
    app.use(cors(corsOptions));
    
    app.use('/api/v1', routes);

    return app;
}
// define a route handler for the default home page


export const start = () => {
    const app = get();
    try {
        app.listen(port, () => {
            console.log(`Server running on http://localhost:${port}`)
        });
    } catch (error: any) {
        console.log(`Error occurred: ${error.message}`)
    }
}

start();
